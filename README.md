# League of Legends Viewer
## Phase 2, Group 3

## Overview

This application should allow users to search for a summoner by name and view information related to that summoner. 


_Important note: in the `Resources/values` folder is a file called `Keys.txt`. You need to copy this file and save as `Keys.xml` and then put your api key where it says `your-api-key-here`. So the .txt file will go unchanged and your api key will be stored in a new .xml file. This xml file will be ignored by version control, but the txt file will not and should remain in version control without any sensitive information._


## 1. Pull Data from API
Base URL = https://na.api.pvp.net/

Reference = https://developer.riotgames.com/api/methods

### Summoners

* Name (/api/lol/{region}/v1.4/summoner/by-name/{summmonerNames}?api_key={yourApiKey}
* Match History (/api/lol/{region}/v2.2/matchlist/by-summoner/{summonerId})
* Match Detail (/api/lol/{region}/v2.2/match/{matchId})
	* Images for Characters
	* Images for Items
                
        _All items endpoint:_ `https://global.api.pvp.net/api/lol/static-data/na/v1.2/item?api_key={yourApiKey}`

        _Item by id with image option:_ `https://global.api.pvp.net/api/lol/static-data/na/v1.2/item/{itemId}?itemData=image&api_key={yourApiKey}`


## 2. Process and Send Data to Views

* Putting correct data together
* Send Correct data to display

## 3. UI/UX

* Navigation
* Displaying images


### _The following information was taken from the requirements PDF that was posted on Moodle:_ 

## Important Information

* Repository access will be emailed out by Wednesday, Feb 8th.
* There will be a code freeze on Monday, February 20th at 11:59 PM.
* You will be presenting at random on Tuesday, February 21st.
* Presentations are limited to 10 minutes and 5 minutes of questions.
* Check your builds on a fresh computer before the code freeze to ensure a painless
process.
* Very Important: ALL presentation materials MUST be committed to the repository.
* The solution should run as soon as it is pulled down from BitBucket!
* You will present on your own laptops, but we reserve the right to require presentation
from our laptop.


## Description

In this phase you will be building mobile app utilizing the [League of Legends API](https://developer.riotgames.com/api/methods). The
app should allow users to search summoners by name, view summoner match history, and
match detail. The match detail should have images for characters and items. You can use [this
website](https://na.op.gg/summoner/userName=R%C3%A9venge) for inspiration.
 If you have any questions about how the technologies work, please ask your TAs. If
you need any more clarification, you can always email Envoc at 383@envoc.com.


## Technology Needed

* Xamarin
* Git Extensions
* BitBucket


## Technical Requirements

* You must sign up for a Riot Developer Account
* You will be building one mobile app, either iOS or Android. You have the option of
targeting Xamarin.iOS, Xamarin.Android, Xamarin.Forms. If you use Xamarin.Forms
only one platform is required.