﻿using System;
using System.Collections.Generic;

namespace LOL23.Models
{
	public class Frame
	{
		public List<Event> events { get; set; }
		public long timestamp { get; set; }
		public List<Tuple<string, ParticipantFrame>> participiantFrames;
	}
}
