﻿using System;
using System.Collections.Generic;

namespace LOL23.Models
{
	public class MatchList
	{
		public List<MatchReference> matches { get; set; }
		public int startIndex { get; set; }
		public int endIndex { get; set; }
		public int totalGames { get; set; }
	}
}
