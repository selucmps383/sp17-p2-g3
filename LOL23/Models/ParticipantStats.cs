﻿using System;
namespace LOL23.Models
{
	public class ParticipantStats
	{

		public bool winner { get; set; }
		public long champLevel { get; set; }
		public long item0 { get; set; }
		public long item1 { get; set; }
		public long item2 { get; set; }
		public long item3 { get; set; }
		public long item4 { get; set; }
		public long item5 { get; set; }
		public long item6 { get; set; }
		public long kills { get; set; }
		public long doubleKills { get; set; }
		public long tripleKills { get; set; }
		public long quadraKills { get; set; }
		public long pentaKills { get; set; }
		public long unrealKills { get; set; }
		public long largestKillingSpree { get; set; }
		public long deaths { get; set; }
		public long assists { get; set; }
		public long totalDamageDealt { get; set; }
		public long totalDamageDealtToChampions { get; set; }
		public long totalDamageTaken { get; set; }
		public long largestCriticalStrike { get; set; }
		public long totalHeal { get; set; }
		public long minionsKilled { get; set; }
		public long neutralMinionsKilled { get; set; }
		public long neutralMinionsKilledTeamJungle { get; set; }
		public long neutralMinionsKilledEnemyJungle { get; set; }
		public long goldEarned { get; set; }
		public long goldSpent { get; set; }
		public int combatPlayerScore { get; set; }
		public int objectivePlayerScore { get; set; }
		public int totalPlayerScore { get; set; }
		public int totalScoreRank { get; set; }
		public int magicDamageDealtToChampions { get; set; }
		public int physicalDamageDealtToChampions { get; set; }
		public int trueDamageDealtToChampions { get; set; }
		public int visionWardsBoughtInGame { get; set; }
		public int sightWardsBoughtInGame { get; set; }
		public int magicDamageDealt { get; set; }
		public int physicalDamageDealt { get; set; }
		public int trueDamageDealt { get; set; }
		public int magicDamageTaken { get; set; }
		public int physicalDamageTaken { get; set; }
		public int trueDamageTaken { get; set; }
		public bool firstBloodKill { get; set; }
		public bool firstBloodAssist { get; set; }
		public bool firstTowerKill { get; set; }
		public bool firstTowerAssist { get; set; }
		public bool firstInhibitorKill { get; set; }
		public bool firstInhibitorAssist { get; set; }
		public long inhibitorKills { get; set; }
		public long towerKills { get; set; }
		public long wardsPlaced { get; set; }
		public long wardsKilled { get; set; }
		public long largestMultiKill { get; set; }
		public long killingSprees { get; set; }
		public long totalUnitsHealed { get; set; }
		public long totalTimeCrowdControlDealt { get; set; }
	}
}