﻿using System;
using System.Collections.Generic;

namespace LOL23.Models
{
	public class Timeline
	{
		public long frameInterval { get; set; }
		public List<Frame> frames { get; set; }
	}
}