﻿namespace LOL23.Models
{
	public class ParticipantTimelineData
	{
		public double tenToTwenty { get; set; }
		public double thirtytoEnd { get; set; }
		public double zeroToTen { get; set; }
	}
}