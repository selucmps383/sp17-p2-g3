﻿using System;
namespace LOL23.Models
{
	public class Player
	{
		public string summonerId { get; set; }
		public string summonerName { get; set; }
		public string matchHistoryUri { get; set; }
		public int profileIcon { get; set; }
	}
}