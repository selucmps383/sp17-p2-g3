﻿using System;
namespace LOL23.Models
{
		public class MatchReference
		{
			public string region { get; set; }
			public string platformId { get; set; }
			public long matchId { get; set; }
			public long champion { get; set; }
			public string queue { get; set; }
			public string season { get; set; }
			public long timestamp { get; set; }
			public string lane { get; set; }
			public string role { get; set; }
		}

}
