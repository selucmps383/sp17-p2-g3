using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Json;
using System.Threading.Tasks;
using System.Net;
using System.IO;

namespace LOL23
{
    [Activity(Label = "MatchDetailActivity")]
    public class MatchDetailActivity : Activity
    {
        ProgressDialog Progress;
        string region, matchId;
        protected override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.Match_Detail);

            TextView detailText = FindViewById<TextView>(Resource.Id.detailView);

            region = Intent.GetStringExtra("Region");
            matchId = Intent.GetStringExtra("Id");

            try
            {
                Progress = new ProgressDialog(this);
                Progress.SetMessage("Please Wait...");
                Progress.SetProgressStyle(ProgressDialogStyle.Spinner);
                Progress.Show();

                string url = "https://" + region + GetString(Resource.String.BaseUrl) +
                                region + GetString(Resource.String.MatchUrl) +
                                matchId + "?api_key=" +
                                GetString(Resource.String.ApiKey);

                JsonValue matchDetail = await FetchMatchDetailAsync(url);

                Progress.Dismiss();

                detailText.Append(matchDetail["participants"][0].ToString());

                //for(int i=0; i<matchDetail["particpants"].Count; i++)
                //    detailText.Append(matchDetail["participants"][i].ToString());
            }
            catch (WebException er)
            {
                Console.Out.WriteLine(er.ToString());
                Toast.MakeText(this, "Something went wrong...", ToastLength.Long).Show();
                Progress.Dismiss();

                var historyIntent = new Intent(this, typeof(MatchHistory));
                historyIntent.PutExtra("Region", region);
                historyIntent.PutExtra("Id", matchId);
                StartActivity(historyIntent);
            }
            
        }


        private async Task<JsonValue> FetchMatchDetailAsync(string url)
        {
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(new Uri(url));
            request.ContentType = "application/json";
            request.Method = "GET";

            using (WebResponse response = await request.GetResponseAsync())
            {
                using (Stream stream = response.GetResponseStream())
                {
                    JsonValue jsonDoc = await Task.Run(() => JsonObject.Load(stream));

                    return jsonDoc;
                }
            }
        }
    }
}