using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Json;
using System.Threading.Tasks;
using System.Net;
using System.IO;

namespace LOL23
{
    [Activity(Label = "MatchHistory")]
    public class MatchHistory : ListActivity
    {
        ProgressDialog Progress;
        String[] matchIds;
        string region;

        protected override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.Match_History);

            region = Intent.GetStringExtra("Region");
            var summonerId = Intent.GetIntExtra("Id", 0);

            try
            {
                Progress = new ProgressDialog(this);
                Progress.SetMessage("Please Wait...");
                Progress.SetProgressStyle(ProgressDialogStyle.Spinner);
                Progress.Show();

                string url = "https://" + region + GetString(Resource.String.BaseUrl) +
                                region + GetString(Resource.String.MatchListUrl) +
                                summonerId + "?api_key=" +
                                GetString(Resource.String.ApiKey);

                // Console.Out.WriteLine(url);

                JsonValue matchListJson = await FetchMatchListAsync(url);
                var matches = matchListJson["matches"];

                matchIds = new String[matches.Count];
                for (int i = 0; i < matches.Count; i++)
                {
                    matchIds[i] = matches[i]["matchId"].ToString();
                }

                Progress.Dismiss();

                ListAdapter = new ArrayAdapter(this, Android.Resource.Layout.SimpleListItem1, matchIds);
            }
            catch (WebException er)
            {
                Progress.Dismiss();
                Console.Out.WriteLine(er.ToString());
                Toast.MakeText(this, "Something went wrong...", ToastLength.Long).Show();
            }


            //Console.Out.WriteLine(matches.ToString());

        }


        protected override void OnListItemClick(ListView l, View v, int position, long id)
        {
            var t = matchIds[position];
            Toast.MakeText(this, t, ToastLength.Short).Show();

            var detailIntent = new Intent(this, typeof(MatchDetailActivity));
            detailIntent.PutExtra("Region", region);
            detailIntent.PutExtra("Id", t);
            StartActivity(detailIntent);
        }


        private async Task<JsonValue> FetchMatchListAsync(string url)
        {
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(new Uri(url));
            request.ContentType = "application/json";
            request.Method = "GET";

            using (WebResponse response = await request.GetResponseAsync())
            {
                using (Stream stream = response.GetResponseStream())
                {
                    JsonValue jsonDoc = await Task.Run(() => JsonObject.Load(stream));

                    return jsonDoc;
                }
            }
        }
    }

}
