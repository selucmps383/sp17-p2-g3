﻿using System;
using Android.App;
using Android.Widget;
using Android.OS;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Json;
using LOL23.Models;

namespace LOL23.Controllers
{
	public class APIController
	{
		// This is the server address of the lol api which does not change 
		private const string serverAddress = "https://na.api.pvp.net/api/lol/na/";
		private Summoner summoner;

		public APIController()
		{
		}
		public APIController(Summoner summoner)
		{
			this.summoner = summoner;
		}

		public Summoner GetSummonerByName(string name)
		{
			Uri path = new Uri(serverAddress + "v1.4/summoner/by-name/" + name + "?api_key=" + Resource.String.ApiKey);

			Summoner result = null; // View -> if summoner != null show all summoner values
									//Call the GetAPICall method to get the Json Object
			Task<JsonValue> jsonDoc = null;
	 		jsonDoc = GetAPICall(path);
			if (jsonDoc != null)
			{
				//Use this stream to build a JSON document object:
				result = JsonConvert.DeserializeObject<Summoner>(jsonDoc.ToString());
			}
			return result;

		}
		public MatchList GetMatchHistory(Summoner summoner)
		{
			Uri path = new Uri(serverAddress + "v2.2/matchlist/by-summoner/" + summoner.id + "?api_key=" + Resource.String.ApiKey);
			MatchList result = null;
			Task<JsonValue> jsonDoc =  GetAPICall(path);
			if (jsonDoc != null)
			{
				result = JsonConvert.DeserializeObject<MatchList>(jsonDoc.ToString());
			}
			return result;
				
			
		}
		public MatchDetail GetMatchDetail(MatchReference match)
		{
			Uri path = new Uri(serverAddress + "v2.2/match/" + match.matchId + "?api_key=" + Resource.String.ApiKey);
			MatchDetail result = null;
			Task<JsonValue> jsonDoc = GetAPICall(path);
			if (jsonDoc != null)
			{
				result = JsonConvert.DeserializeObject<MatchDetail>(jsonDoc.ToString());
			}
			return result;
		}


		// APICALL METHOD
		public async Task<JsonValue> GetAPICall(Uri path)
		{ 
			HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(path);
			request.ContentType = "application/json";
			request.Method = "GET";
			// Send the request to the server and wait for the response:
			using (WebResponse response = await request.GetResponseAsync())
			{
				// Get a stream representation of the HTTP web response:
				using (Stream stream = response.GetResponseStream())
				{
					JsonValue jsonDoc = null;
					var result = await Task.Run(() => JsonValue.Load(stream));
					if (result != null) 
					{
						jsonDoc = result;
						return jsonDoc;
					}
					return result;
				}
			}
		}
	}
}
