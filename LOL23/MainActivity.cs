﻿using System;
using Android.App;
using Android.Widget;
using Android.OS;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Json;
using Android.Content;

namespace LOL23
{
    [Activity(Label = "LOL Summoner Viewer", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        ProgressDialog Progress;
        TextView sInfo;
        string region = "na";

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.Main);
            //ImageView imageView = FindViewById<ImageView>(Resource.Id.icon1);
            EditText summonerSearchBar = FindViewById<EditText>(Resource.Id.SearchBar);
            Button summonerSearchButton = FindViewById<Button>(Resource.Id.SearchButton);
            Spinner regionSpinner = FindViewById<Spinner>(Resource.Id.regionSpinner);

            regionSpinner.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(regionSpinner_ItemSelected);
            var adapter = ArrayAdapter.CreateFromResource(
                this, Resource.Array.region_array, Android.Resource.Layout.SimpleSpinnerItem);

            adapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            regionSpinner.Adapter = adapter;


            sInfo = FindViewById<TextView>(Resource.Id.sinfo);


            summonerSearchButton.Click += async (sender, e) =>
            {

                region = regionSpinner.SelectedItem.ToString().ToLower();
                string url = "https://" + region + GetString(Resource.String.BaseUrl) +
                                region + GetString(Resource.String.SummonerUrl) +
                                summonerSearchBar.Text + "?api_key=" +
                                GetString(Resource.String.ApiKey);

                Console.Out.WriteLine(url);


                try
                {

                    Progress = new ProgressDialog(this);
                    Progress.SetMessage("Please Wait...");
                    Progress.SetProgressStyle(ProgressDialogStyle.Spinner);
                    Progress.Show();

                    JsonValue summonerJson = await FetchSummonerAsync(url);
                    JsonValue summonerResults = summonerJson[summonerSearchBar.Text];

                    var summonerActivity = new Intent(this, typeof(SummonerInfo));
                    summonerActivity.PutExtra("Region", region);
                    summonerActivity.PutExtra("Name", summonerResults["name"].ToString());
                    summonerActivity.PutExtra("Id", (int)summonerResults["id"]);
                    summonerActivity.PutExtra("ProfileIconId", (int)summonerResults["profileIconId"]);
                    summonerActivity.PutExtra("SummonerLevel", (int)summonerResults["summonerLevel"]);

                    Progress.Dismiss();
                    StartActivity(summonerActivity);

                }
                catch (WebException er)
                {
                    Progress.Dismiss();
                    Console.Out.WriteLine(er.ToString());
                    Toast.MakeText(this, "Not Found. \nTry Another Search", ToastLength.Long).Show();
                    summonerSearchBar.Text = "";
                }

                
            };

            


            

        }

        private void regionSpinner_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            Spinner spinner = (Spinner)sender;

            string toast = string.Format("The region is {0}", spinner.GetItemAtPosition(e.Position));          
        }


        // This is basically from the WeatherApp tutorial from Xamarin
        private async Task<JsonValue> FetchSummonerAsync (string url)
        {
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(new Uri(url));
            request.ContentType = "application/json";
            request.Method = "GET";

            using (WebResponse response = await request.GetResponseAsync())
            {
                using (Stream stream = response.GetResponseStream())
                {
                    JsonValue jsonDoc = await Task.Run(() => JsonObject.Load(stream));
                    return jsonDoc;
                }
            }
        }
    }
}
