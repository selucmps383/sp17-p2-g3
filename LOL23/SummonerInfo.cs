using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Square.Picasso;
using System.Json;
using System.Threading.Tasks;
using System.Net;
using System.IO;

namespace LOL23
{
    [Activity(Label = "SummonerInfo")]
    public class SummonerInfo : Activity
    {
        ProgressDialog Progress;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.SummonerInfo);

            Button MatchHistory = FindViewById<Button>(Resource.Id.matchHistory);
            TextView sumData = FindViewById<TextView>(Resource.Id.summonerInfo);
            TextView nameText = FindViewById<TextView>(Resource.Id.nameText);
            TextView summonerLevelText = FindViewById<TextView>(Resource.Id.summonerLevelText);
            ImageView summonerIcon = FindViewById<ImageView>(Resource.Id.profileImage);


            var region = Intent.GetStringExtra("Region");
            var summonerId = Intent.GetIntExtra("Id", 0);
            nameText.Text = Intent.GetStringExtra("Name") ?? "Data not available";
            summonerLevelText.Text = Intent.GetIntExtra("SummonerLevel", 0).ToString();
            var profileIconId = Intent.GetIntExtra("ProfileIconId", 0);
            string profileIconUrl = "http://ddragon.leagueoflegends.com/cdn/7.3.3/img/profileicon/" + profileIconId + ".png";

            Picasso.With(this)
                .Load(profileIconUrl)
                .Into(summonerIcon);

            MatchHistory.Click += (sender, e) =>
            {
                var MatchHistoryIntent = new Intent(this, typeof(MatchHistory));
                MatchHistoryIntent.PutExtra("Region", region);
                MatchHistoryIntent.PutExtra("Id", (int)summonerId);
                StartActivity(MatchHistoryIntent);
            };
        }

        //private async Task<JsonValue> FetchMatchListAsync(string url)
        //{
        //    HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(new Uri(url));
        //    request.ContentType = "application/json";
        //    request.Method = "GET";

        //    using (WebResponse response = await request.GetResponseAsync())
        //    {
        //        using (Stream stream = response.GetResponseStream())
        //        {
        //            JsonValue jsonDoc = await Task.Run(() => JsonObject.Load(stream));

        //            return jsonDoc;
        //        }
        //    }
        //}

    }
}
